import  express  from 'express'
import { urlencoded, json } from 'body-parser'
import DotEnv from './config/dotenv.config'
import cors from 'cors'
import routes from './routes'

const app = express()

app.use('/api',routes);
app.use(urlencoded({extended: true}))
app.use(json())
app.use(cors())       

DotEnv.config()

//console.log(process.env);

app.listen(3001,()=>{
    console.log('Server is running PORT 3001')    
});
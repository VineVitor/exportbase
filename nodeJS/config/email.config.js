const emailConfig = {
    transporter : {
        host: 'smtp.gmail.com',
        //port: 587,
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: 'noreply@vinesolucoes.com.br',
            pass: 'n0r3plyV1n3'
        },
        tls: { rejectUnauthorized: true }
    },
    from : 'VINE SOLUCOES <noreply@vinesolucoes.com.br>'
}
export default emailConfig
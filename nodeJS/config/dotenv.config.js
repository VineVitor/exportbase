import dotenv from 'dotenv'

export default class DotEnv {
       static config = () => {
              return dotenv.config({
                     path: process.env.NODE_ENV === "test" ? ".env" : ".env.development"
              })
       }
}
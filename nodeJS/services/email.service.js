import nodemailer from 'nodemailer'
import emailConfig  from '../config/email.config'

export default class EmailService
{
    static sender(emails,subject,bodyHtml, attachments = [])
    {
        const transporter = nodemailer.createTransport(emailConfig.transporter)

        const mailOptions = {
            from: emailConfig.from,
            to: emails,
            subject: subject,
            html: bodyHtml,
            attachments : attachments,
            // bbc: 'gustavo.lopes@vinesolutions.com.br'
        }

        return new Promise((resolve,reject)=>{
            transporter.sendMail(mailOptions, (error, info)=>{
                if (error)
                  reject(error)
                else
                   resolve(`Email enviado:  ${info.response}`)
            })
        })
    }
}
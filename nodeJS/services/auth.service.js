// import jwt from 'jsonwebtoken'
// import securityKey from '../config/auth'
import jwt_decode  from 'jwt-decode'

export default class AuthService
{
    static decodeJWT(token)
    {
        return jwt_decode(token);
    }
}
import { createConnection } from 'mysql'
import stream from 'stream'

export default class DbService
{
  static result = []

  static conn(){

    const dbConfig = {
        host : process.env.DB_HOST,
        port : process.env.DB_PORT,
        user : process.env.DB_USER,
        password : process.env.DB_PASSWORD || '',
        database :  process.env.DB_DATABASE,
        supportBigNumbers: true,
        // multipleStatements: true
    }
    const connection = createConnection(dbConfig)

    return new Promise((resolve,reject)=>{
        connection.connect((err)=>{
            if(err)
                reject(err)
            else
                resolve(connection)
        })
    })
    
  }

  // static async query(sqlQry){
    
  //   const db = await this.conn()

  //   return new Promise((resolve,reject)=>{
  //       db.query(sqlQry, (error, results, fields) => {

  //           if(error) 
  //               reject(error)
  //           else
  //               resolve(results)

  //           db.end()
  //       });
  //   })
  // }

  static async query(sqlQry){
    
    const db = await this.conn()
    let resultRows = []

    return new Promise((resolve,reject)=>{
      
      db.query(sqlQry)
        .on('error', (err) =>{
          reject(err)
        })
        .stream()
        .pipe(new stream.Transform({
          objectMode: true,
          transform : (row, encoding, callback) =>{
            resultRows.push(row)
            callback()
          }
        }))
        .on('finish', ()=> {
          db.end()
          resolve(resultRows)
        })
    })
  }

}
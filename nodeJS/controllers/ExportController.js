
import Utils from '../core/utils'
import DbService from '../services/db.service'
import AuthService from '../services/auth.service'
import EmailService from '../services/email.service'
import { Mailtemplate }  from '../templates/exportBase-template'
import String  from '../strings/index';
import path from 'path'

import fs from 'fs'
const DIR = path.resolve(__dirname)


export default class ExportController
{
    static index(req,res){
        let decoded = AuthService.decodeJWT(String.jwt)
        return res.json({success:true,message:'seja bem vindo a fgf',user: decoded})
    }

    // static async senderEmail(req,res){
    //     // const attachments = {
    //     //     filename: 'baseexport.sql',
    //     //     path: `${DIR}/baseexport.sql` // stream this file
    //     // }
    //     const resp = await EmailService.sender('gustavo.lopes@vinesolutions.com.br','E-mail enviado usando Node!','<h3>Bem fácil, não?</h3>')
    //     return res.json({success: true, message: resp})
    // }

    static async products(req,res){

        let antes = Date.now();
        // const resp = await DbService.query('select COUNT(*) from products')
        const resp = await DbService.query('select * from products')
        let duracao = Date.now() - antes;
        // console.log("levou " + duracao + "ms")

        const filePath = `${DIR}/products.csv`
        const file = fs.createWriteStream(filePath,{emitClose: true})

        file.write(Utils.convertToCSV(resp),err=>{
            console.log(err)

            if(!err){
                Utils.compressGZIP(filePath)
            }
        })

        //Utils.compressGZIP(filePath)

        // try{
        //     fs.writeFileSync(`${DIR}/products.csv`,Utils.convertToCSV(resp),'utf8')
        // }catch(ex){
        //     console.error("Error:",ex)
        // }

        // fs.writeFile(`${DIR}/products.json`, JSON.stringify(resp), 'utf8',  (err) =>{
        //     if (err) {
        //         console.log("An error occured while writing JSON Object to File.");
        //         return console.log(err);
        //     }else{

        //         console.log("JSON file has been saved.");
        //         Utils.compressGZIP(`${DIR}/products.json`)
        //     }
        // });
        //file.end()
        //console.log(resp.length)
        //Utils.compressGZIP(`${DIR}/products.csv`)

        res.json({success: true,message:'file created'})
    }

    static async senderEmail(req,res){
        const resp = await EmailService.sender('vitor.vicente@diegofranco.net','E-mail enviado usando Node!',Mailtemplate)
        return res.json({success: true, message: resp})
    }
}
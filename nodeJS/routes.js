import express from 'express'
const routes = express.Router()

import ExportController from './controllers/ExportController'

routes.get('/',ExportController.index)
routes.get('/products',ExportController.products)


export default routes 
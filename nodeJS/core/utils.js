import gzipme from 'gzipme'
import path from 'path'

import { convertArrayToCSV,converter } from 'convert-array-to-csv'

const DIR = path.resolve(__dirname)

export default class Utils
{
    static compressGZIP(pathFile)
    {
        gzipme(pathFile, false, 'best')
    }
    static convertToCSV(dataObjects)
    {
        return convertArrayToCSV(dataObjects)
    }
}
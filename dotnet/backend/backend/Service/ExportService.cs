using backend.CommunicationModels;
using backend.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace backend.Service
{
    public class ExportService
    {

        public DefaultResponseModel GenerateFiles(string directoryName,string webRootPath)
        {
            string newPath = webRootPath+"/"+directoryName;
            var files = new List<FileConvertModel>();

            var rev_segmento = DbConn.QueryDataSet("SELECT * FROM rev_segmento");
            var rev_subsegmento = DbConn.QueryDataSet("SELECT * FROM rev_subsegmento");

            var range = new int[] {0,315835};

            var _conn = DbConn.Connection;
            _conn.Open();

            for(int i = 0; i < 3; i++)
            {
                var products = DbConn.QueryDataSet(_conn,@"
                    SELECT id, CONVERT(`ean`,char) as EAN,
                        name,type, CONCAT(n,'.',c,'.', m) AS ncm,
                        lessEAN as EAN_FALSO,
                        subsegment_al,
                        subsegment_am,
                        subsegment_ba,
                        subsegment_es, 
                        subsegment_go,
                        subsegment_mg,
                        subsegment_ms,
                        subsegment_pa,
                        subsegment_pb,
                        subsegment_pe,
                        subsegment_rj, 
                        subsegment_rn,
                        subsegment_sp
                        FROM products LIMIT @start, @end",new {start = range[0], end = range[1]});
                    range[0] += range[1];

                files.Add( new FileConvertModel
                {
                    Name = "products_"+(i+1)+".csv",
                    Path = newPath + "/products_"+(i+1)+".csv",
                    StrCSV = products.ConvertToCSV(),
                });
            }
            _conn.Clone();

            files.Add( new FileConvertModel
            {
                Name = "rev_segmento.csv",
                Path = newPath + "/rev_segmento.csv",
                StrCSV = rev_segmento.ConvertToCSV(),
            });

            files.Add(new FileConvertModel
            {
                Name = "rev_subsegmento.csv",
                Path = newPath + "/rev_subsegmento.csv",
                StrCSV = rev_subsegmento.ConvertToCSV(),
            });

            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            string zipStart = webRootPath+"/compress";

            if (!Directory.Exists(zipStart))
            {
                Directory.CreateDirectory(zipStart);
            }

            Utils.CreateFIleCSV(files); 

            ZipDirectoryFiles(zipStart,webRootPath+"/compress.zip");

            Utils.DeleteDirectory(newPath);
            Utils.DeleteDirectory(webRootPath+"/compress");

            return new DefaultResponseModel
            {
                Sucesso = true,
                Mensagem = "Gerado com sucesso!",
                Retorno = rev_segmento
            };
        }

        public void ZipDirectoryFiles(string startPath,string zipPath)
        {
   
            System.IO.File.Delete(zipPath);

            using (ZipArchive archive = ZipFile.Open(zipPath, ZipArchiveMode.Create))
            {
                var directorySelected = new DirectoryInfo(startPath);

                foreach (FileInfo fileToCompress in directorySelected.GetFiles())
                {
                    archive.CreateEntryFromFile(fileToCompress.FullName, fileToCompress.Name);
                }
            }
        }
    }
}
﻿using backend.CommunicationModels;
using backend.Core;
using backend.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace backend.Service
{
    public class AccountService
    {
        private readonly IConfiguration _configuration;
     

        public AccountService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public LoginResponseModel Login(LoginRequestModel Model)
        {
            if(Model == null)
            {
                return new LoginResponseModel
                {
                    Error = true,
                    Data = "object is not null"
                };
            }

            // Autenticando usuario
            var user = DbConn.Find<User>("select * from fin_usuarios WHERE user_login = @login and user_pwd = @senha",
            new {
                login = Model.Login.ToLower(),
                senha = Utils.CalculateSHA1(Model.Senha)
            });
   
            if (user == null)
            {
                return new LoginResponseModel 
                { 
                    Error = true, 
                    Data = "Usuário ou senha incoretos"
                };
            }

            var signingKey = Convert.FromBase64String(_configuration["SecurityKey"]);
            var limit = DateTime.UtcNow.AddHours(1);

            var claims = new Claim[]
            {
               new Claim(ClaimTypes.Name,user.User_nome),
               new Claim(ClaimTypes.Email,user.User_email),
               new Claim(ClaimTypes.Role,user.User_perfil.ToString()),
               new Claim("expires", limit.ToString("yyyy-MM-dd HH:mm:ss")),
            };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = null,
                Audience = null,
                IssuedAt = DateTime.UtcNow,
                NotBefore = DateTime.UtcNow,
                Expires = limit,
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(signingKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = jwtTokenHandler.CreateJwtSecurityToken(tokenDescriptor);
            string _token = jwtTokenHandler.WriteToken(jwtToken);

            return new LoginResponseModel
            {
                Token = _token,
                Data = "Usuário autenticado com sucesso",
                Expires = limit.ToString("yyyy-MM-dd HH:mm:ss")
            };
        }

        public JwtResponseModel ValidateAndDecode(string jwt)
        {
            var validationParameters = new TokenValidationParameters
            {
               ValidateIssuer = false,
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(_configuration["SecurityKey"]))
            };

            try
            {
                var claimsPrincipal = new JwtSecurityTokenHandler()
                    .ValidateToken(jwt, validationParameters, out var rawValidatedToken);

                //return (JwtSecurityToken)rawValidatedToken;
                return new JwtResponseModel
                {
                    Valid = true
                };
            }
            catch (SecurityTokenValidationException stvex)
            {
                return new JwtResponseModel
                {
                    Valid = false,
                    Mensagem = $"Token failed validation: {stvex.Message}"
                };
            }
            catch (ArgumentException argex)
            {
                return new JwtResponseModel
                {
                    Valid = false,
                    Mensagem = $"Token was invalid: {argex.Message}"
                };
            }
        }        
    }
}

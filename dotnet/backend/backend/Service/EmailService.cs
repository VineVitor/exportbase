using backend.CommunicationModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace backend.Service
{
    public class EmailService
    {
        public DefaultResponseModel Sender(string Emails, string subject,string Message,List<string> files = null)
        {
            try
            {
                // Credentials
                var credentials = new NetworkCredential("noreply@vinesolucoes.com.br", "n0r3plyV1n3");
                
                // Mail message
                var mail = new MailMessage()
                {
                    From = new MailAddress("noreply@vinesolucoes.com.br"),
                    Subject = subject,
                    Body = Message
                };
                mail.IsBodyHtml = true;

                var listEmail = Emails.Split(";");

                foreach(string email in listEmail)
                {
                    mail.To.Add(new MailAddress(email));
                }

                if(files != null)
                {
                    foreach(var fileName in files)
                    {
                        mail.Attachments.Add(new Attachment(fileName));
                    }
                }
              
                // Smtp client
                var client = new SmtpClient()
                {
                    Port = 587,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = "smtp.gmail.com",
                    EnableSsl = true,
                    Credentials = credentials,
                    Timeout = 200000 //200 segundos
                };
                client.Send(mail);
                
                return new DefaultResponseModel
                {
                    Mensagem = "Email send successfully!",
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

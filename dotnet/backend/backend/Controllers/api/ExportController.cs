﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using ExcelDataReader;
using System.Data;
using System.IO.Compression;
using MySql.Data.MySqlClient;
using System.Text;
using backend.Core;
using Microsoft.Extensions.Configuration;
using backend.CommunicationModels;
using backend.Service;
using backend.Core.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace backend.Controllers.api
{
    [Route("api/[controller]")]
    public class ExportController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private AccountService _accountService;
        private ExportService _exportService;
        
        public ExportController(IHostingEnvironment hostingEnvironment, IConfiguration config, AccountService accountService, ExportService exportService) 
        {
            _hostingEnvironment = hostingEnvironment;
            _accountService = accountService;
            _exportService = exportService;
        }


        [HttpGet]
        [Route("[action]")]
        [Authorize]
        public async Task<IActionResult> Download([FromRoute] string token)
        {
            try
            {
                string zipPath = _hostingEnvironment.WebRootPath+"/compress.zip";
                var result = _exportService.GenerateFiles("csv_temp",_hostingEnvironment.WebRootPath);

                if(result.Sucesso)
                {
                    var memory = new MemoryStream();
                    using (var stream = new FileStream(zipPath, FileMode.Open))
                    {
                        await stream.CopyToAsync(memory);
                    }
                    memory.Position = 0;
                        
                    return File(memory, Utils.GetContentType(zipPath), "compress.zip");
                }

                return BadRequest(new DefaultResponseModel
                {
                    Sucesso = false,
                    Mensagem = "Falha ao gerar arquivos!"
                });
               
            }catch(Exception ex)
            {
                throw ex;
            }         
        }

    }
}

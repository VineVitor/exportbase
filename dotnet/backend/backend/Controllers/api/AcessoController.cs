﻿using backend.CommunicationModels;
using backend.Core;
using backend.Core.Mvc;
using backend.Models;
using backend.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace backend.Controllers.api
{
    [Route("api/[controller]")]
    public class AcessoController : BaseController
    {
        private readonly AccountService _accountService;

        public AcessoController(AccountService accountService)
        {
            _accountService = accountService;
        }


        //[AllowAnonymous]
        [HttpPost]
        [Route("[action]")]
        public IActionResult Login([FromBody] LoginRequestModel Model)
        {
            try
            {
                return Ok(_accountService.Login(Model));

            }catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

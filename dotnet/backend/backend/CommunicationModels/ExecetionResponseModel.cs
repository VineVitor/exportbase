﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.CommunicationModels
{
    public class ExecetionResponseModel
    {
        public string Mensagem { get; set; }
        public Exception Descricao { get; set; }
    }
}

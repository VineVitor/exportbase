﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.CommunicationModels
{
    public class FileConvertModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string StrCSV { get; set; }
    }
}

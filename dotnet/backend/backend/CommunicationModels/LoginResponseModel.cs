﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.CommunicationModels
{
    public class LoginResponseModel
    {
        public bool Error { get; set; } = false;
        public string Data { get; set; }
        public string Token { get; set; }      
        public string Expires { get; set; }        
    }
}

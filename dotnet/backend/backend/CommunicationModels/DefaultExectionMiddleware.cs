﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace backend.CommunicationModels
{
    public class DefaultExectionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<DefaultExectionMiddleware> _logger;

        public DefaultExectionMiddleware(ILogger<DefaultExectionMiddleware> logger,RequestDelegate next)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var response = context.Response;
            int statusCode = (int) HttpStatusCode.InternalServerError;

            response.ContentType = "application/json";
            response.StatusCode = statusCode;

            _logger.LogError(exception, "Exceção capturada.");

            await response.WriteAsync(JsonConvert.SerializeObject(new 
            {
                Sucesso = false,
                Mensagem = exception.Message,
                Descricao = exception
            }));
        }
    }
}

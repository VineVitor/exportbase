using backend.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    public class User 
    {
        public int User_pk { get; set; }
        public int User_pes_fk { get; set; }
        public string User_login { get; set; }
        public string User_pwd { get; set; }
        public string User_nome { get; set; }
        public string User_email { get; set; }
        public string User_descricao { get; set; }
        public string Perfil_administrativo { get; set; }
        public string User_ativo { get; set; }
        public string User_token { get; set; }
        public string User_perfil { get; set; }
    }
}

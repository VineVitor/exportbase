using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace backend.Core
{
    public static class DbConn
    {
        public static IConfiguration Config {get; set;}

        public static MySqlConnection Connection
        {
            get
            {
                string connString = Config["ConnectionStrings:DefaultConnection"];
                var connection = new MySqlConnection(connString);

                return connection;
            }
        }    

        public static List<T> Query<T>(string sqlStr,object parameters = null)
        {
           using (var conn = Connection)
            {
                var result =  conn.Query<T>(sqlStr,parameters);
                
                return result.ToList();
            }
        }

        public static DataSet QueryDataSet(string sqlStr, object parameters = null)
        {
           using (var conn = Connection)
            {
                var result = conn.Query<object>(sqlStr,parameters).ToList();

                return result.ConvertToDataSet();
            }
        }

        public static DataSet QueryDataSet(MySqlConnection conn, string sqlStr,object parameters = null)
        {
            if(conn.State == ConnectionState.Open)
            {
                var result = conn.Query<object>(sqlStr,parameters).ToList();

                return result.ConvertToDataSet();
            }
            return null;
        }

        public static T Find<T>(string sqlStr,object parameters = null)
        {
           using (var conn = Connection)
            {
                return conn.QueryFirstOrDefault<T>(sqlStr,parameters);
            }
        }
        
    }
}

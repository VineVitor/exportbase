using backend.CommunicationModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace backend.Core
{
    public static class Utils
    {
        public static string ConvertToCSV(this DataSet objDataSet)
        {
            var content = new StringBuilder();

            if (objDataSet.Tables.Count >= 1)
            {
                DataTable table = objDataSet.Tables[0];

                if (table.Rows.Count > 0)
                {
                    DataRow dr1 = (DataRow) table.Rows[0];
                    int intColumnCount = dr1.Table.Columns.Count;
                    int index=1;

                    //add column names
                    foreach (DataColumn item in dr1.Table.Columns)
                    {
                        content.Append(String.Format("\"{0}\"", item.ColumnName));
                        if (index < intColumnCount)
                            content.Append(",");
                        else
                            content.Append("\r\n");
                        index++;
                    }

                    //add column data
                    foreach (DataRow currentRow in table.Rows)
                    {
                        string strRow = string.Empty;
                        for (int y = 0; y <= intColumnCount - 1; y++)
                        {
                            strRow += "\"" + currentRow[y].ToString() + "\"";

                            if (y < intColumnCount - 1 && y >= 0)
                                strRow += ",";
                        }
                        content.Append(strRow + "\r\n");
                    }
                }
            }

            return content.ToString();
        }

        // public static void Compress(DirectoryInfo directorySelected, string webRootPath)
        // {
        //     foreach (FileInfo fileToCompress in directorySelected.GetFiles())
        //     {
        //         using (FileStream originalFileStream = fileToCompress.OpenRead())
        //         {
        //             if ((System.IO.File.GetAttributes(fileToCompress.FullName) & 
        //             FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
        //             {
        //                 using (FileStream compressedFileStream = System.IO.File.Create(fileToCompress.FullName + ".gz"))
        //                 {
        //                     using (GZipStream compressionStream = new GZipStream(compressedFileStream, 
        //                     CompressionMode.Compress))
        //                     {
        //                         originalFileStream.CopyTo(compressionStream);
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }

        public static void Compress(string fileName)
        {
            var fileToCompress = new FileInfo(fileName);

            using (FileStream originalFileStream = fileToCompress.OpenRead())
            {
                var fullName = fileToCompress.FullName.Replace("csv_temp","compress");

                if ((System.IO.File.GetAttributes(fileToCompress.FullName) & 
                FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                {
                    using (FileStream compressedFileStream = System.IO.File.Create(fullName + ".gz"))
                    {
                        using (GZipStream compressionStream = new GZipStream(compressedFileStream, 
                            CompressionMode.Compress))
                        {
                            originalFileStream.CopyTo(compressionStream);
                        }
                    }
                }
            }
        }

        public static void CreateFIleCSV(string csvString, string fileName,string pathRoot)
        {
            if (System.IO.File.Exists(fileName))    
            {    
                System.IO.File.Delete(fileName);    
            } 

            // Create a new file     
            using (FileStream fs = System.IO.File.Create(fileName))     
            {      
                Byte[] csv = new UTF8Encoding(true).GetBytes(csvString);    
                fs.Write(csv, 0, csv.Length);    
            } 
            Compress(fileName);
        }

        public static void CreateFIleCSV(List<FileConvertModel> files)
        {
            foreach(var file in files)
            {
                if (System.IO.File.Exists(file.Path))    
                {    
                    System.IO.File.Delete(file.Path);    
                } 
                // Create a new file     
                using (FileStream fs = System.IO.File.Create(file.Path))     
                {      
                    Byte[] csv = new UTF8Encoding(true).GetBytes(file.StrCSV);    
                    fs.Write(csv, 0, csv.Length);    
                } 
                Compress(file.Path);
            }
        }

        public static string GetContentType(string path)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;

            if(!provider.TryGetContentType(path, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        public static void DeleteDirectory(string path)
        {
            var directory = new DirectoryInfo(path);

            foreach (FileInfo file in directory.GetFiles())
            {
                file.Delete();
            }
            Directory.Delete(path);
        }

        public static DataSet ConvertToDataSet<T>(this List<T> list)  
        {  
            Type elementType = typeof(T);  
            DataSet ds = new DataSet();  
            DataTable t = new DataTable();  
            ds.Tables.Add(t);  
  
            //add a column to table for each public property on T   
            foreach (var propInfo in elementType.GetProperties())  
            {  
                t.Columns.Add(propInfo.Name, propInfo.PropertyType);  
            }  
  
            //go through each property on T and add each value to the table   
            foreach(T item in list)   
            {  
                DataRow row = t.NewRow(); foreach (var propInfo in elementType.GetProperties())  
                {   
                    row[propInfo.Name] = propInfo.GetValue(item, null);   
                }  
                t.Rows.Add(row);  
            }  
            return ds;  
        }

        public static string CalculateSHA1(string value = "")
        {
           try
            {
                byte[] buffer = Encoding.Default.GetBytes(value);
                System.Security.Cryptography.SHA1CryptoServiceProvider cryptoTransformSHA1 =  new System.Security.Cryptography.SHA1CryptoServiceProvider();
                string hash = BitConverter.ToString(cryptoTransformSHA1.ComputeHash(buffer)).Replace("-", "");
                return hash;
            }
            catch (Exception x)
            {
                throw new Exception(x.Message);
            }
        }
    }
}

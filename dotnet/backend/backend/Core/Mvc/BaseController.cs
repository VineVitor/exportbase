﻿using backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using System.Data;

namespace backend.Core.Mvc
{
    public class BaseController : ControllerBase
    {  

        protected User UserOnline
        {
            get
            {
                foreach (string token1 in Request.Headers["Authorization"])
                {
                    if (token1 != "")
                    {
                        var stream = token1.Replace("Bearer", "").Trim();
                        var handler = new JwtSecurityTokenHandler();
                        var jsonToken = handler.ReadToken(stream);
                        var tokenS = handler.ReadToken(stream) as JwtSecurityToken;

                        //var jti = tokenS.Claims.First(claim => claim.Type == "jti").Value
                    }
                }
                return null;
            }
        }
    }
}

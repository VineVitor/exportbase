﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using backend.CommunicationModels;
using backend.Core;
using backend.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using Serilog;


namespace backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            // Init Serilog configuration
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //config db
            //string connection = Configuration["ConnectionStrings:DefaultConnection"];
            // services.AddDbContext<AppDbContext>(options =>
            //     options.UseMySql(connection)
            // );
            // services.AddScoped<ProductDAO>();
            DbConn.Config = Configuration;

            services.AddTransient<EmailService>();
            services.AddTransient<ExportService>();
            services.AddTransient<AccountService>();

            services.AddCors(options =>
            {
                // options.AddPolicy("AllowAllHeaders",
                //     builder =>
                //        {
                //            builder.WithOrigins("http://localhost:8080")
                //            .AllowAnyHeader()
                //            .AllowAnyMethod()
                //            .AllowCredentials();
                //        });

                    options.AddPolicy("devPolicy",
                        builder =>
                        {
                            builder.AllowAnyOrigin()
                                .AllowAnyHeader()
                                .AllowAnyMethod()
                                .AllowCredentials();
                        });
            });

            // Add framework services.
            services.AddMvc()
            .AddJsonOptions(json => {
                json.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            //services.AddTransient<AccountService>();

            // JWT
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(options =>
               {
                   var signingKey = Convert.FromBase64String(Configuration["SecurityKey"]);

                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       ValidateIssuer = false,
                       ValidateAudience = false,
                       ValidateIssuerSigningKey = true,
                       IssuerSigningKey = new SymmetricSecurityKey(signingKey)
                   };
               }).AddCookie();

                services.Configure<IISOptions>(o => 
                {
                    o.ForwardClientCertificate = false;
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            // logging
            loggerFactory.AddSerilog();

            app.UseAuthentication();
            app.UseCors("devPolicy");

            app.UseMiddleware<CommunicationModels.DefaultExectionMiddleware>();

            //InicializeDb.Initialize(context);

            // app.Use(async (context1, next) => {
            //     await next();
            //     if (context1.Response.StatusCode == 404 &&
            //         !Path.HasExtension(context1.Request.Path.Value) &&
            //         !context1.Request.Path.Value.StartsWith("/api/"))
            //     {
            //         context1.Request.Path = "/index.html";
            //         await next();
            //     }
            // });
                       
            app.UseMvcWithDefaultRoute();
            app.UseDefaultFiles();
            app.UseStaticFiles();
           
            app.UseMvc();
        }
    }
}
